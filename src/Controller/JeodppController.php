<?php
/**
 * @file
 * Contains \Drupal\jeodpp_map\Controller\JeodppController.
 */
 
namespace Drupal\jeodpp_map\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines JeodppController class.
 */
class JeodppController {
	
  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('<div id="map-container" style="height: 500px;"></div>'),
    );
  }
  
}