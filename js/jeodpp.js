var jeodppAfterMap ;
var globalJeodppBeforeDate;
var globalJeodppAfterDate;

function enableJeodppMenu(jeodppBeforeMap, options){
	options.menuContainer === undefined ? options.menuContainer = '#jeodpp-menu-container' : options.menuContainer;
	options.sliderContainer === undefined ? options.sliderContainer = '#jeodpp-menu-container' : options.sliderContainer;
	console.log(options);
	var latmin, latmax, lonmin, lonmax;
	var maxTiles = 12;
	//https://jeodpp.jrc.ec.europa.eu/jiplib-view-dev/?imagejsonlist=true&lat=43.5&lon=12.25&datemin=2019-08-01&datemax=2019-09-01
	var jeodppImageService = 'https://proxy.biopama.org/https://jeodpp.jrc.ec.europa.eu/jiplib-view/?imagejsonlist=true';
	
	//https://jeodpp.jrc.ec.europa.eu/jiplib-view-dev/?dynapi=true&lonmin=12&lonmax=12.5&latmin=43&latmax=44&datemin=2019-08-01&datemax=2019-09-01&cloud=80&limit=2&r=B04&g=B03&b=B02&x=34&y=23&z=6
	var jeodppWMS = 'https://proxy.biopama.org/https://jeodpp.jrc.ec.europa.eu/jiplib-view/?dynapi=true';
	
	/*
	Define an object that stores all values from the form
	*/
	var jeodpp = {
		lat: null,
		lon: null,
		startDate: null,
		endDate: null,
		dateSelected: null,
		opacityBefore: 1,
		opacityAfter: 1,
		offsetType: 'month',
		offsetNum: 1,
		offsetStartDate: null,
		offsetEndDate: null,
		offsetDateSelected: null,
		bands: "r=B04&g=B03&b=B02",
		cloudCover: 50
	};
	//r=B04&g=B03&b=B02
	
	//wrap the original map in a div that will hold the JEODPP menu
	jQuery(jeodppBeforeMap._container).wrap( "<div id='jeodpp-menu-container'></div>" );
	
	//create the form for the menu
	var jeodppMenu = document.createElement("div");
	jeodppMenu.setAttribute('id',"jeodpp-menu");
	jeodppMenu.setAttribute('class',"hidden");

	var jeodppDates = document.createElement("div"); //date range
	jeodppDates.setAttribute('name',"dates");
	jeodppDates.setAttribute('id',"jeodpp-daterange");
	jeodppDates.setAttribute('class',"form-control");
	
	jQuery(jeodppDates).append("<span></span>");

	var jeodppCloudCover = document.createElement("div"); //cloud cover slider
	jeodppCloudCover.setAttribute('name',"cloud_cover");
	jeodppCloudCover.setAttribute('id',"jeodpp-cloud-cover");
	
	var jeodppBands = document.createElement("select"); //RGB wrapper
	jeodppBands.setAttribute('id',"jeodpp-bands");
	jeodppBands.setAttribute('class',"form-control");
	
	var jeodppBandsDesc = document.createElement("div"); //RGB wrapper
	jeodppBandsDesc.setAttribute('id',"jeodpp-bands-desc");
	
	jeodppBands.innerHTML = "<option value='natural'>Natural color - B4/B3/B2</option>"+
		"<option value='infrared'>Infrared color - B8/B4/B3</option>"+
		"<option value='agriculture'>Agriculture - B11/B8A/B2</option>"+
		"<option value='swir'>Short-wave Infrared Imagery (SWIR) - B12/B8/B4</option>"+
		"<option value='forest'>Forest Short-wave Infrared Imagery (SWIR) - B11/B8/B4</option>"+
		"<option value='ndvi'>Normalized Difference Vegetation Index (NDVI) - B8-B4/B8+B4</option>"+
		"<option value='nwi'>Normalized Water Difference (NWI) - B3-B8/B3+B8</option>";
	
	jQuery(jeodppMenu).append("<h5>Date range for Sentinel 2 Imagery</h5>");
	jeodppMenu.appendChild(jeodppDates);
	jQuery(jeodppMenu).append("<h5>Cloud Cover (%)</h5>");
	jeodppMenu.appendChild(jeodppCloudCover);
	jQuery(jeodppMenu).append("<h5>Select a visualization</h5>");
	jeodppMenu.appendChild(jeodppBands);
	jeodppMenu.appendChild(jeodppBandsDesc);
	
	//create the slider for the images
	var jeodppImageSliderWrapper = document.createElement("div");
	jeodppImageSliderWrapper.setAttribute('id',"jeodpp-image-slider-wrapper");
	jeodppImageSliderWrapper.setAttribute('class',"hidden");
	
	var jeodppImageSliderBeforeWrapper = document.createElement("div");
	jeodppImageSliderBeforeWrapper.setAttribute('class',"jeodpp-image-slider-wrapper");
	jeodppImageSliderBeforeWrapper.setAttribute('id',"jeodpp-image-slider-before-wrapper");
	
	var jeodppImageSliderBefore = document.createElement("div"); //image slider Before
	jeodppImageSliderBefore.setAttribute('id',"jeodpp-image-slider-before");
	jeodppImageSliderBefore.setAttribute('class',"jeodpp-image-slider");
	
	var jeodppOpacitySliderBefore = document.createElement("div"); //opacity slider Before
	jeodppOpacitySliderBefore.setAttribute('id',"jeodpp-opacity-slider-before");
	jeodppOpacitySliderBefore.setAttribute('class',"jeodpp-opacity-slider");
	
	jeodppImageSliderBeforeWrapper.appendChild(jeodppImageSliderBefore);
	jeodppImageSliderBeforeWrapper.appendChild(jeodppOpacitySliderBefore);
	jQuery(jeodppImageSliderBeforeWrapper).append("<div class='jeodpp-opacity-label'>Opacity</div>");
	
	jeodppImageSliderWrapper.appendChild(jeodppImageSliderBeforeWrapper);
	
	//append the JEODPP menu and image slider to the map
	jQuery(options.menuContainer).append(jeodppMenu);
	jQuery(options.sliderContainer).append(jeodppImageSliderWrapper);
	
		/*
	Define the Date range picker
	http://www.daterangepicker.com/#usage 
	*/
	
	var start = moment().subtract(29, 'days');
    var end = moment();

    function dateRangeCallback(start, end, label) {
        jQuery('#jeodpp-daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		jeodpp.startDate = start.format('YYYY-MM-DD');
		jeodpp.endDate = end.format('YYYY-MM-DD');
		updateImageList();
		if (options.compare == true) rebuildOffsetSlider();
    }

	jQuery('#jeodpp-daterange').daterangepicker({
		"showDropdowns": true,
		"autoApply": true,
		"maxSpan": {
			"days": 30
		},
		ranges: {
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		"alwaysShowCalendars": true,
		"startDate": moment().subtract(29, 'days'),
		"endDate": moment(),
		"opens": "center"
	}, dateRangeCallback);

    dateRangeCallback(start, end);
	
	jQuery('#daterange').on('apply.daterangepicker', function(ev, picker) {
	  updateImageList();
	  if (options.compare == true) updateImageList("after");
	});
	
	if (options.compare == true){
		var jeodppImageSliderOffset = document.createElement("div");
		jeodppImageSliderOffset.setAttribute('id',"jeodpp-slider-offset");
		
		var jeodppOffsetType = document.createElement("select"); 
		jeodppOffsetType.setAttribute('id',"jeodpp-offset-type");
		jeodppOffsetType.innerHTML = "<option value='month'>Month(s)</option>"+
			"<option value='year'>Year(s)</option>";
		var jeodppOffsetNum = document.createElement("select"); 
		jeodppOffsetNum.setAttribute('id',"jeodpp-offset-num");
		jeodppOffsetNum.innerHTML = "<option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option>";
		
		jQuery(jeodppImageSliderOffset).append("<h5 class='jeodppoffset noselect'>Compare to </h5>");
		jeodppImageSliderOffset.appendChild(jeodppOffsetNum);
		jeodppImageSliderOffset.appendChild(jeodppOffsetType);
		jQuery(jeodppImageSliderOffset).append("<h5 class='jeodppoffset noselect'> ago</h5><br>");
		
		var jeodppImageSliderAfterWrapper = document.createElement("div");
		jeodppImageSliderAfterWrapper.setAttribute('class',"jeodpp-image-slider-wrapper");
		jeodppImageSliderAfterWrapper.setAttribute('id',"jeodpp-image-slider-after-wrapper");
		
		var jeodppImageSliderAfter = document.createElement("div"); //image slider After
		jeodppImageSliderAfter.setAttribute('id',"jeodpp-image-slider-after");
		jeodppImageSliderAfter.setAttribute('class',"jeodpp-image-slider");
		
		var jeodppOpacitySliderAfter = document.createElement("div"); //opacity slider After
		jeodppOpacitySliderAfter.setAttribute('id',"jeodpp-opacity-slider-after");
		jeodppOpacitySliderAfter.setAttribute('class',"jeodpp-opacity-slider");
		
		jeodppImageSliderAfterWrapper.appendChild(jeodppImageSliderAfter);
		jeodppImageSliderAfterWrapper.appendChild(jeodppOpacitySliderAfter);
		jQuery(jeodppImageSliderAfterWrapper).append("<div class='jeodpp-opacity-label noselect'>Opacity</div>");
		
		jeodppImageSliderWrapper.appendChild(jeodppImageSliderOffset);
		jeodppImageSliderWrapper.appendChild(jeodppImageSliderAfterWrapper);
		
		//create a duplicate of the original map and stick it next to the first one to create a comparison
		var jeodppCompareMapContainer = document.createElement("div"); //compare map container
		jeodppCompareMapContainer.setAttribute('id',"jeodpp-compare-map");
		jQuery("#jeodpp-menu-container").append(jeodppCompareMapContainer);
		var sourceNode = document.getElementById(jeodppBeforeMap._container.id);
		
		copyNodeStyle(sourceNode, jeodppCompareMapContainer)
		function copyNodeStyle(sourceNode, targetNode) {
		  var computedStyle = window.getComputedStyle(sourceNode);
		  Array.from(computedStyle).forEach(function (key) {
			return targetNode.style.setProperty(key, computedStyle.getPropertyValue(key), computedStyle.getPropertyPriority(key));
		  });
		}
		var currentZoom = jeodppBeforeMap.getZoom();
		var currentCenter = jeodppBeforeMap.getCenter();
		//var currentMapStyle = jeodppBeforeMap.getStyle();
		
		jeodppAfterMap = new mapboxgl.Map({
		container: 'jeodpp-compare-map',
		style: 'mapbox://styles/jamesdavy/cjw25laqe0y311dqulwkvnfoc',
		center: currentCenter,
		zoom: currentZoom
		});
		var map = new mapboxgl.Compare(jeodppBeforeMap, jeodppAfterMap, {
		// Set this to enable comparing two maps by mouse movement:
		// mousemove: true
		});
		
		var jeodppOpacityAfterSlider = document.getElementById('jeodpp-opacity-slider-after');	
		noUiSlider.create(jeodppOpacityAfterSlider, {
			start: 100,
			step: 1,
			range: {
				min: 0,
				max: 100
			}
		});
		jeodppOpacityAfterSlider.noUiSlider.on('update', function (values) { 
			jeodpp.opacityAfter = parseInt(values[0], 10) / 100;
			if (jeodppAfterMap.getLayer('jeodpp-layer')){
				jQuery("#jeodpp-compare-map").css("opacity", jeodpp.opacityAfter, "important");
			}
		});
		jQuery("#jeodpp-offset-type").change(function () {
			jeodpp.offsetType = jQuery(this).val();
			rebuildOffsetSlider();
		});
		
		jQuery("#jeodpp-offset-num").change(function () {
			jeodpp.offsetNum = jQuery(this).val();
			rebuildOffsetSlider();
		});
		
		var afterDateSlider = document.getElementById('jeodpp-image-slider-after');
		noUiSlider.create(afterDateSlider, {
			range: {
				min: timestamp(jeodpp.offsetStartDate),
				max: timestamp(jeodpp.offsetEndDate)
			},
			format: {
				to: function (value) {
					return moment(value).format('MMMM D, YYYY');
				},
				from: function (value) {
					return value;
				}
			},
			tooltips: true,
			step: 1 * 24 * 60 * 60 * 1000,// Steps by day
			start: timestamp(jeodpp.offsetStartDate)
		});
	}
	function rebuildOffsetSlider() {
		jeodpp.offsetStartDate = moment(jeodpp.startDate, 'YYYY-MM-DD').subtract(jeodpp.offsetNum, jeodpp.offsetType).format('YYYY-MM-DD');
		jeodpp.offsetEndDate = moment(jeodpp.endDate, 'YYYY-MM-DD').subtract(jeodpp.offsetNum, jeodpp.offsetType).format('YYYY-MM-DD');
		updateImageList("after");
	}
	
	//jeodppBands.addEventListener('change', updateJeodppRGB());
	jQuery("#jeodpp-bands").change(function () {
		var currentVal = jQuery(this).val();
		switch(currentVal) {
		  case 'natural':
		    jeodpp.bands = "r=B04&g=B03&b=B02";
			break;
		  case 'forest':
		    jeodpp.bands = "r=B11&g=B08&b=B04";
			break;
		  case 'infrared':
		    jeodpp.bands = "r=B08&g=B04&b=B03";
			break;
		  case 'agriculture':
		    jeodpp.bands = "r=B11&g=B8A&b=B02";
			break;
		  case 'swir':
		    jeodpp.bands = "r=B12&g=B08&b=B04";
			break;
		  case 'ndvi':
		    jeodpp.bands = "r=NDVI";
			break;
		  case 'nwi':
		    jeodpp.bands = "r=NWI";
			break;
		  default:
			jeodpp.bands = "r=B04&g=B03&b=B02";
		}
		if (jeodppBeforeMap.getLayer('jeodpp-layer')){
			updateJeodppImages();
			if (options.compare == true) updateJeodppImages("after");
		}
    });
	
	/*
	Define the cloud cover slider
	http://ionden.com/a/plugins/ion.rangeSlider/index.html
	*/
	
	//settings needed to format the dates in the image slider    
	var lang = "en-US";
    
    function dateToTS (date) {
        return date.valueOf();
    }
    
    function tsToDate (ts) {
        var d = new Date(ts);
    
        return d.toLocaleDateString(lang, {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        });
    }
	var jeodppCloudCoverSlider = document.getElementById('jeodpp-cloud-cover');	
	noUiSlider.create(jeodppCloudCoverSlider, {
		start: 50,
		step: 1,
		range: {
			min: 0,
			max: 100
		},
		tooltips: true,
		pips: {
			mode: 'values',
			values: [0, 25, 50, 75, 100],
			density: 5
		}
	});
	
	var jeodppOpacityBeforeSlider = document.getElementById('jeodpp-opacity-slider-before');	
	noUiSlider.create(jeodppOpacityBeforeSlider, {
		start: 100,
		step: 1,
		range: {
			min: 0,
			max: 100
		}
	});
	jeodppOpacityBeforeSlider.noUiSlider.on('update', function (values) { 
		jeodpp.opacityBefore = parseInt(values[0], 10) / 100;
		if (jeodppBeforeMap.getLayer('jeodpp-layer')){
			jeodppBeforeMap.setPaintProperty('jeodpp-layer', 'raster-opacity', jeodpp.opacityBefore);
		}
	});

	jeodppCloudCoverSlider.noUiSlider.on('change', function (values, handle, unencoded, tap, positions) {
		jeodpp.cloudCover = unencoded;
		updateImageList();
		if (options.compare == true) updateImageList("after");
	});
	

	var beforeDateSlider = document.getElementById('jeodpp-image-slider-before');
	noUiSlider.create(beforeDateSlider, {
		range: {
			min: timestamp(jeodpp.startDate),
			max: timestamp(jeodpp.endDate)
		},
		format: {
			to: function (value) {
				return moment(value).format('MMMM D, YYYY');
			},
			from: function (value) {
				return value;
			}
		},
		tooltips: true,
		step: 1 * 24 * 60 * 60 * 1000,// Steps by day
		start: timestamp(jeodpp.startDate)
	});
	
	function timestamp(str) {
		return new Date(str).getTime();
	}

	/*
	Update list of available JEODPP images
	*/
	function updateImageList(map = "before") {
		var startDate;
		var endDate;
		if(map == "after"){
			startDate = jeodpp.offsetStartDate;
			endDate = jeodpp.offsetEndDate;
		} else {
			startDate = jeodpp.startDate;
			endDate = jeodpp.endDate;
		}
		if (jeodpp.lat !== null){
			var jeodppImageServiceURL = jeodppImageService + '&lat=' + jeodpp.lat + '&lon=' + jeodpp.lon + '&datemin=' + startDate + '&datemax=' + endDate;
			
			jQuery.ajax({
				url: jeodppImageServiceURL,
				mapPosition: map,
				startDate: startDate,
				endDate: endDate,
				success: jeodppResponsParser,
				dataType: "json",
				error: function (jqXHR, tranStatus, errorThrown) {
					jQuery("div#jeodpp-errors").addClass("alert-danger").html('<div>Status: ' + jqXHR.status + ' ' + jqXHR.statusText + '. ' +
					'Response: ' + jqXHR.responseText + '</div>');
				}
			});
		}
    }
	
	function jeodppResponsParser(d) {
		if (!jQuery("#jeodpp-images-wrapper").length){
			jQuery( '<div id="jeodpp-images-wrapper"></div>' ).appendTo( "div#jeodpp-menu" );
		} else {
			jQuery('#jeodpp-images-wrapper').empty();
		}
		if (d.length == 0){
			jQuery('#jeodpp-image-slider-' + this.mapPosition).hide();
			jQuery('#jeodpp-no-images-error').remove();
			jQuery( '<h5 id="jeodpp-no-images-error">No Images found from ' + this.startDate + ' to ' + this.endDate + ' @ Lat: ' + jeodpp.lat + ' Lon: ' + jeodpp.lon + '</h5>').appendTo('#jeodpp-image-slider-wrapper');
		} else {
			var zoom = jeodppBeforeMap.getZoom();
			if (zoom < 9) {jeodppBeforeMap.flyTo({center: [jeodpp.lon, jeodpp.lat], zoom: 9});}
			var bboxSize = 0.4;
			latmin = +jeodpp.lat - +bboxSize;
			latmax = +jeodpp.lat + +bboxSize;
			lonmin = +jeodpp.lon - +bboxSize;
			lonmax = +jeodpp.lon + +bboxSize;
			jQuery('#jeodpp-no-images-error').remove();
			jQuery('#jeodpp-image-slider-' + this.mapPosition).show();
			jQuery( '<h5>Images from ' + this.startDate + ' to ' + this.endDate + ' @ Lat: ' + jeodpp.lat + ' Lon: ' + jeodpp.lon + '</h5><select id="jeodpp-images-dropdown" class="form-control" name="Satellite Images"></select>' ).appendTo('#jeodpp-images-wrapper');
			var dropdown = jQuery('select#jeodpp-images-dropdown');
			dropdown.empty();

			//var jeodppCurrentImages = {};
			var imageDates = [];
			var oids = [];
			var cloudcoverValues = [];
			jQuery.each(d, function( index, value ) {
				d[index].beginposition = d[index].beginposition.split('T')[0];
			    dropdown.append(jQuery('<option></option>')
				.text("OID: " + d[index].oid + ' | ' + "Beginposition: " + d[index].beginposition + ' | ' + "Platform Serial Identifier: " + d[index].platformserialidentifier + ' | ' + "Cloud Cover: " + d[index].cloudcover));
				//var currentOID  = d[index].oid
				imageDates.push(d[index].beginposition);
				oids.push(d[index].oid);
				//jeodppCurrentImages.imageDate = date.split('T')[0];
				cloudcoverValues.push(d[index].cloudcover);
			});

			var imageDatesAsTimestamp = [];
			var filteredImageDates = [];
			var filteredOIDs = [];
			jQuery.each(imageDates, function( index, value ) {
				if (cloudcoverValues[index] < jeodpp.cloudCover){
					imageDatesAsTimestamp.push(timestamp(imageDates[index]));
					filteredImageDates.push(imageDates[index]);
					filteredOIDs.push(oids[index]);
				} 
			});
			console.log(filteredOIDs)
			var slider;
			var map;
			if( this.mapPosition == "after" ){
				slider = afterDateSlider;
				map = jeodppAfterMap;
			} else {
				slider = beforeDateSlider;
				map = jeodppBeforeMap;
			}
			
			slider.noUiSlider.updateOptions({
				range: {
					min: timestamp(this.startDate),
					max: timestamp(this.endDate)
				},
				pips: {
					mode: 'values',
					values: imageDatesAsTimestamp,
					stepped: true,
					density: 4,
					format: {
						to: function (value) {
							return moment(value).format('MMM D, YYYY');
						}
					},
				},
			});
			currentPips = slider.getElementsByClassName('noUi-value-large');
			
			for (var i = 0; i < currentPips.length; i++) {
				//currentPips[i].jeodppOID = filteredOIDs[i];
				currentPips.item(i).setAttribute("jeodppOID", filteredOIDs[i]);
			}
			console.log(currentPips);
			
			
			//since we just loaded the new values to the date slider. Select the most recent date by default and pull the images.
			slider.noUiSlider.set(imageDatesAsTimestamp[0]);
			if( this.mapPosition == "after" ){
				jeodpp.offsetDateSelected = filteredImageDates[0];
				if (globalJeodppAfterDate != jeodpp.offsetDateSelected){
					globalJeodppAfterDate = jeodpp.offsetDateSelected;
					updateJeodppImages("after");
				}
			} else {
				jeodpp.dateSelected = filteredImageDates[0];
				if (globalJeodppBeforeDate != jeodpp.dateSelected){ 
					globalJeodppBeforeDate = jeodpp.dateSelected;
					updateJeodppImages();
				}
			}
			
			jQuery("#jeodpp-image-slider-" + this.mapPosition + " .noUi-value.noUi-value-horizontal.noUi-value-large").hover(
			  function() {
				var jeodppDateHovered = jQuery( this ).text();
				jQuery(this).addClass('jeodpp-date-hovered');
				var jeodppDateHoveredFormatted = moment(jeodppDateHovered,'MMM D, YYYY').format('YYYY-MM-DD');
				
				var jeodppMapLayerURL = jeodppWMS + '&lonmin=' + lonmin + '&lonmax=' + lonmax + '&latmin=' + latmin + '&latmax=' + latmax + '&datemin=' + jeodppDateHoveredFormatted+ '&datemax=' + jeodppDateHoveredFormatted +  '&cloud=' + jeodpp.cloudCover + '&limit=' + maxTiles + '&r=FR&g=FG&b=FB&x={x}&y={y}&z={z}';
				
				if (map.getLayer('jeodpp-layer-hover')){
					map.removeLayer('jeodpp-layer-hover');
				}

				if (map.getSource('jeodpp-layer-hover')){
					map.removeSource('jeodpp-layer-hover');
				}
				map.addLayer({
					'id': 'jeodpp-layer-hover',
					'type': 'raster',
					'source': {
					'id': 'jeodpp-source',
					'type': 'raster',
					'tiles': [jeodppMapLayerURL],
					'tileSize': 256
					},
					'paint': {}
				}, options.mapLayer);	
			  }, function() {
				jQuery('.jeodpp-date-hovered').removeClass('jeodpp-date-hovered');
				if (map.getLayer('jeodpp-layer-hover')) map.removeLayer('jeodpp-layer-hover');
				if (map.getSource('jeodpp-layer-hover')) map.removeSource('jeodpp-layer-hover');
			  }
			);
			jQuery(".noUi-value").click(
				function() {
					var sliderParent = jQuery(this).closest(".jeodpp-image-slider").attr("id");
					jQuery('.jeodpp-date-selected').removeClass("jeodpp-date-selected");
					jQuery( this ).addClass("jeodpp-date-selected");
					var jeodppDateHovered = jQuery( this ).text();
					jQuery( this ).removeClass("jeodpp-date-hovered");
					var jeodppDateHoveredFormatted = moment(jeodppDateHovered,'MMM D, YYYY').format('x');
					if( sliderParent == "jeodpp-image-slider-after" ){
						afterDateSlider.noUiSlider.set(jeodppDateHoveredFormatted);
						jeodpp.offsetDateSelected = moment(jeodppDateHoveredFormatted, 'x').format('YYYY-MM-DD');
						updateJeodppImages("after");
					} else {
						beforeDateSlider.noUiSlider.set(jeodppDateHoveredFormatted);
						jeodpp.dateSelected = moment(jeodppDateHoveredFormatted, 'x').format('YYYY-MM-DD');
						updateJeodppImages();
					}
				}
			);
		}
	}
	function updateJeodppImages(mapPosition = "before"){
		var date;
		var map;
		if (mapPosition == "after"){
			date = jeodpp.offsetDateSelected;
			map = jeodppAfterMap;
		} else {
			var date = jeodpp.dateSelected;
			var map = jeodppBeforeMap;
		}
		var jeodppMapLayerURL = jeodppWMS + '&lonmin=' + lonmin + '&lonmax=' + lonmax + '&latmin=' + latmin + '&latmax=' + latmax + '&datemin=' + date + '&datemax=' + date +  '&cloud=' + jeodpp.cloudCover + '&limit=' + maxTiles + '&' + jeodpp.bands + '&x={x}&y={y}&z={z}';
		
		if (map.getLayer('jeodpp-layer')){
			map.removeLayer('jeodpp-layer');
		}

		if (map.getSource('jeodpp-layer')){
			map.removeSource('jeodpp-layer');
		}
		map.addLayer({
			'id': 'jeodpp-layer',
			'type': 'raster',
			'source': {
			'id': 'jeodpp-source',
			'type': 'raster',
			'tiles': [jeodppMapLayerURL],
			'tileSize': 256
			},
			'paint': {}
		}, options.mapLayer);		
		if(mapPosition == "before"){
			jeodppBeforeMap.setPaintProperty('jeodpp-layer', 'raster-opacity', jeodpp.opacityBefore);
		}
	}
	
	function openJeodppMenu(event) {
	  if (jQuery( "button.button-jeodpp-menu" ).hasClass( "toggle-on" )) {
			jQuery('button.button-jeodpp-menu').removeClass( "toggle-on" );
			jQuery('div#jeodpp-menu').hide('fast');
		} else {
			jQuery('button.button-jeodpp-menu').addClass( "toggle-on" );
			jQuery('div#jeodpp-menu').removeClass( "hidden" );
			jQuery('div#jeodpp-menu').show('fast');
		}
	}
	
	function openJeodppSliderMenu(event) {
	  if (jQuery( "button.button-jeodpp-slider" ).hasClass( "toggle-on" )) {
			jQuery('button.button-jeodpp-slider').removeClass( "toggle-on" );
			jQuery("#jeodpp-image-slider-wrapper").hide('fast');
		} else {
			jQuery('button.button-jeodpp-slider').addClass( "toggle-on" );
			jQuery('div#jeodpp-image-slider-wrapper').removeClass( "hidden" );
			jQuery("#jeodpp-image-slider-wrapper").show('fast');
		}
	}
	
	/* Idea from Stack Overflow https://stackoverflow.com/a/51683226  */
	class MapboxGLButtonControl {
	  onAdd(map) {
		this._btn = document.createElement("button");
		this._btn.className = "mapboxgl-ctrl-icon button-jeodpp-menu";
		this._btn.type = "button";
		this._btn.title = "Toggle JEODPP Menu";
		this._btn.innerHTML = "<i class='fab fa-grav'></i>"; 
		this._btn.onclick = openJeodppMenu;
		
		this._btn2 = document.createElement("button");
		this._btn2.className = "mapboxgl-ctrl-icon button-jeodpp-slider";
		this._btn2.type = "button";
		this._btn2.title = "Toggle JEODPP Image slider";
		this._btn2.innerHTML = "<i class='fas fa-ruler-horizontal'></i>"; 
		this._btn2.onclick = openJeodppSliderMenu;

		this._container = document.createElement("div");
		this._container.className = "mapboxgl-ctrl-group mapboxgl-ctrl jeodpp-mapboxgl-ctrl jeodpp-menu";
		this._container.appendChild(this._btn);
		this._container.appendChild(this._btn2);

		return this._container;
	  }

	  onRemove() {
		this._container.parentNode.removeChild(this._container);
		this._map = undefined;
	  }
	}
	
	const jeodppMenuToggle = new MapboxGLButtonControl();
	
	var controlMap;
	if (options.compare == true) {
		controlMap = jeodppAfterMap;
		jeodppAfterMap.on('click', function (e) {
			updateMenuAndImages(e);
		});
	} else {
		controlMap = jeodppBeforeMap;
	}
	
	controlMap.addControl(jeodppMenuToggle, "top-right");
	jQuery('button.button-jeodpp-slider').hide();//do not show the slider until the map is clicked.
	
	/*
	Select a point in the map
	*/
	jeodppBeforeMap.on('click', function (e) {
		updateMenuAndImages(e);
	});

	function updateMenuAndImages(e){
		jeodpp.lat = parseFloat(e.lngLat.lat).toFixed(4);
		jeodpp.lon = parseFloat(e.lngLat.lng).toFixed(4);
		updateImageList();
		if (options.compare == true) updateImageList("after");
		jQuery('button.button-jeodpp-slider').show().addClass( "toggle-on" );
		jQuery('div.jeodpp-slider-toggle').addClass( "toggle-on" );
		jQuery('div#jeodpp-image-slider-wrapper').removeClass( "hidden" );
		jQuery("#jeodpp-image-slider-wrapper").show('fast'); 		
	}
}