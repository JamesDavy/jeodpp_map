var tTipDelay = 100;
jQuery(document).ready(function($) {
	var mapPolyHostURL = "https://tiles.biopama.org/BIOPAMA_poly_2";
	var mapPaLayer = "WDPA2019MayPoly";
	mapboxgl.accessToken = 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg';
	var map = new mapboxgl.Map({
		container: 'map-container',
		style: 'mapbox://styles/jamesdavy/cjw25laqe0y311dqulwkvnfoc', //Andrews default new RIS v2 style based on North Star
		attributionControl: true,
		renderWorldCopies: true,
		center: [0, -6.66],
        zoom: 1.5,
		minZoom: 1.4,
		maxZoom: 18
	}).addControl(new mapboxgl.AttributionControl({
        customAttribution: "JEODPP test.",
		compact: true
    }));
	map.on('load', function () {
		map.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 18,
		});
		map.addLayer({
			"id": "osm_test",
			"type": "line",
			"source": {
				"type": "vector",
				"tiles": ["https://europa.eu/webtools/maps/tiles/vtserver/cntr-2013/{z}/{x}/{y}"],
				"minzoom": 0,
				"maxzoom": 18
			},
			"source-layer": "cntr-2013",
			"layout": {
				"line-cap": "round",
				"line-join": "round"
			},
			"paint": {
				"line-opacity": 0.6,
				"line-color": "rgb(53, 175, 109)",
				"line-width": 2
			}
		});
		map.addLayer({
			"id": "wdpaAcpFill",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"minzoom": 1,
			"paint": {
				"fill-color": [
					"match",
					["get", "MARINE"],
					["1"],
					"hsla(173, 21%, 51%, 0.2)",
					"hsla(87, 47%, 53%, 0.2)"
				],
				"fill-opacity": [
                    "interpolate",
                    ["exponential", 1],
                    ["zoom"],
                    3,
                    0.3,
                    5,
                    0.5,
                    6,
                    1
                ]
			}
		}, 'state-label-lg');
		map.addLayer({
			"id": "wdpaAcpHover",
			"type": "line",
			"source": "BIOPAMA_Poly",
			"minzoom": 4,
			"source-layer": mapPaLayer,
			"paint": {
				"line-color": "#8fc04f",
				"line-width": 1,
				"line-opacity": [
                    "interpolate",
                    ["exponential", 1],
                    ["zoom"],
                    3,
                    0.3,
                    5,
                    0.5,
                    6,
                    1
                ]
			}
		}, 'state-label-lg');
		
		var jeodppOptions = {
			compare: true, //turn on compare function
			mapLayer: "wdpaAcpHover", //which layer will the sentinel map load under
			menuContainer: "#jeodpp-menu-container", // ID of the container that will hold the main menu - if not declared it defaults to "#jeodpp-menu-container"
			sliderContainer: "#jeodpp-menu-container" // ID of the container that will hold the sliders for dates and opacity - if not declared it defaults to "#jeodpp-menu-container"
		};
		//If you only want a simple base map in the compare map, it's easy, just use this:
		//enableJeodppMenu(map, jeodppOptions);
		
		//if you want to do all kinds of stuff you use this (I wrap it in ajax as it's an easily readable way to use a js promise)
		$.ajax({
			url: enableJeodppMenu(map, jeodppOptions),
			success:function(){
				// IMPORTANT -- The compare map is a global variable called jeodppAfterMap
				// Here is an example for adding additional layers to the after map.
				jeodppAfterMap.addSource("BIOPAMA_Poly", {
					"type": 'vector',
					"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
					"minZoom": 0,
					"maxZoom": 18,
				});
				jeodppAfterMap.addLayer({
					"id": "wdpaAcpHover",
					"type": "line",
					"source": "BIOPAMA_Poly",
					"minzoom": 4,
					"source-layer": mapPaLayer,
					"paint": {
						"line-color": "#8fc04f",
						"line-width": 1,
						"line-opacity": [
							"interpolate",
							["exponential", 1],
							["zoom"],
							3,
							0.3,
							5,
							0.5,
							6,
							1
						]
					}
				});
			}
		})
	});
	
	
	var windowHeight = getWindowHeight();
	resizeMap(windowHeight);
	
	
	$("#jeodpp-menu-container").css({ 'height': windowHeight + "px" });
	
	function getWindowHeight(){
		var height = $(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
		if ($('#toolbar-item-administration-tray')[0]){
			var adminHeight = $('#toolbar-item-administration-tray').height() + $('#toolbar-bar').height();
			$('#map-container').css('top', adminHeight);
			height = height - adminHeight;
		}
		return height;
	}
	function resizeMap(height){
		$('#map-container').css('height', height);
		map.resize();
	}

});




